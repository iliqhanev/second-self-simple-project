Responsive website using Boostrap Grid System. The code is left as its first written, giving the ability to check my progress through the projects development.
* Used:
* Bootstrap 3.3.7;
* jQuery;
* Lightbox.js;
* Fontawesome icons;
* Vanilla JavaScript visualization and CSS manipulation;
